# Hellfest schedule to Pentabarf XML

This is a (quickly hacked) script fetching the Hellfest 2022 schedule
and converting it into a Pentabarf XML file, compatible with suitable
[mobile/desktop apps](https://fosdem.org/2022/schedule/mobile/).

It has only been tested with [Confy](https://confy.kirgroup.net/).

# Hellfest 2022 schedule

This script is run on an hourly basis on my own server, with its output
being publicly available at https://blog.a-wai.com/hf2022.xml

# License

This script is licensed under the MIT license.
