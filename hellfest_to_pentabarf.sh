#!/bin/bash

set -e

OUTPUT="$1"
if [ -z "$OUTPUT" ]; then
    echo "Usage: $0 OUTPUT_FILE"
    exit 1
fi

TMPDIR=$(mktemp -d /tmp/hellfest.XXXXXXXX)

# Obtained from monitoring iOS app requests, it might change
# during the festival and **will** change next year
URL="https://goevent.s3.amazonaws.com/hellfest/2022/8f9225492a854a6caf45b770bc1127d5/latest/schedule.zip"

SQL_REQUEST="SELECT Venues.title, Artists.title, Artists.description, Artists.style,
                    Shows.ref_date, Shows.date_start, Shows.time_start, Shows.date_end, Shows.time_end
             FROM Shows INNER JOIN Venues ON Shows.venue_id=Venues._id
                         INNER JOIN Artists ON Shows.object_id=Artists._id
             ORDER BY Shows.ref_date, Shows.date_start, Shows.time_start;"

ZIP_FILE="$TMPDIR/schedule.zip"
# The zip contains 2 almost identical files:
# - schedule_0.sqlite: descriptions in French
# - schedule_1.sqlite: decriptions in English
DB_FILE="$TMPDIR/schedule_0.sqlite"

wget -O "$ZIP_FILE" "$URL"
unzip -o "$ZIP_FILE" -d "$TMPDIR/"

# Having newlines in descriptions will mess up the script as we expect each entry
# to be a single line, make sure we don't have such chars in those fields
sqlite3 "$DB_FILE" "UPDATE Artists SET description = REPLACE(description, CHAR(10),'')"

# Dump concerts list
DB_OUTPUT=$(sqlite3 "$DB_FILE" "$SQL_REQUEST" | sed 's/ & / AND /g')

# When calculating the duration we don't want to be influenced by the timezone
# otherwise the calculated duration would be wrong (real_duration + utc_offset)
export TZ="UTC"

CUR_DAY=""
DAY_ID=1
EVENT_ID=1

echo "<schedule>" > $OUTPUT
echo "  <conference>" >> $OUTPUT
echo "    <title>Hellfest 2022</title>" >> $OUTPUT
echo "    <subtitle/>" >> $OUTPUT
echo "    <venue/>" >> $OUTPUT
echo "    <city>Clisson</city>" >> $OUTPUT
echo "    <start>2022-06-16</start>" >> $OUTPUT
echo "    <end>2022-06-27</end>" >> $OUTPUT
echo "    <days>8</days>" >> $OUTPUT
echo "    <day_change>06:00:00</day_change>" >> $OUTPUT
echo "    <timeslot_duration>00:05:00</timeslot_duration>" >> $OUTPUT
echo "  </conference>" >> $OUTPUT

while read _LINE; do
    while IFS='|' read -ra _SHOW; do
        STAGE="${_SHOW[0]}"
        ARTIST="${_SHOW[1]}"
        DESC="$(echo ${_SHOW[2]} | sed -e 's/<span[^>]*>//g' -e 's%</span>%%g')"
        GENRE="${_SHOW[3]}"
    
        DAY="${_SHOW[4]}"
        DAY_START="${_SHOW[5]}"
        DAY_END="${_SHOW[7]}"
        TIME_START="${_SHOW[6]}"
        TIME_END="${_SHOW[8]}"
        # Calculate duration in a quick & dirty way
        TS_START=$(date --date="$DAY_START $TIME_START" +%s)
        TS_END=$(date --date="$DAY_END $TIME_END" +%s)
        DURATION=$(date --date="@$(($TS_END-$TS_START))" +"%H:%M")
        # Format the start time without seconds
        TIME_START=$(date --date="@$TS_START" +"%H:%M")
        
        if [ "$DAY_START" != "$CUR_DAY" ]; then
            if [ $DAY_ID -gt 1 ]; then
                echo "  </day>" >> $OUTPUT
            fi
            echo "  <day index=\"$DAY_ID\" date=\"$DAY_START\">" >> $OUTPUT
            let DAY_ID=$DAY_ID+1
            CUR_DAY=$DAY_START
        fi
        
        echo "    <room name=\"$STAGE\">" >> $OUTPUT
        echo "      <event id=\"$EVENT_ID\">" >> $OUTPUT
        echo "        <start>$TIME_START</start>" >> $OUTPUT
        echo "        <duration>$DURATION</duration>" >> $OUTPUT
        echo "        <room>$STAGE</room>" >> $OUTPUT
        echo "        <slug/>" >> $OUTPUT
        echo "        <title>$ARTIST</title>" >> $OUTPUT
        echo "        <subtitle/>" >> $OUTPUT
        echo "        <track>$GENRE</track>" >> $OUTPUT
        echo "        <type>maintrack</type>" >> $OUTPUT
        echo "        <language/>" >> $OUTPUT
        echo "        <abstract/>" >> $OUTPUT
        echo "        <description>$DESC</description>" >> $OUTPUT
        echo "        <persons> </persons>" >> $OUTPUT
        echo "        <attachments> </attachments>" >> $OUTPUT
        echo "        <links> </links>" >> $OUTPUT
        echo "      </event>" >> $OUTPUT
        echo "    </room>" >> $OUTPUT
    done <<< "$_LINE"
    let EVENT_ID=$EVENT_ID+1
done <<< "$DB_OUTPUT"

echo "  </day>" >> $OUTPUT
echo "</schedule>" >> $OUTPUT

rm -rf "$TMPDIR"
